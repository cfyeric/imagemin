const mozjpeg = require('imagemin-mozjpeg');
const pngquant = require('imagemin-pngquant');
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');

gulp.task('default', () => {
  gulp.src('images/*')
    .pipe(imagemin([
      pngquant({quality: [0.1, 0.1]}),
      mozjpeg({quality: 70})
    ]))
    .pipe(gulp.dest('dist/images'))
});